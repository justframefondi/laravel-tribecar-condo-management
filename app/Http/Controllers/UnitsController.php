<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unit;

class UnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
            $units = Unit::all();

            return view('units.index', [
                'units' => $units
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $id = $request->input('unit_block') . '-' . $request->input('unit_floor') . '-' . $request->input('unit_number');

        $unit = Unit::create([
            'id' => strtoupper($id),
            'unit_block' => $request->input('unit_block'),
            'unit_number' => $request->input('unit_number'),
            'unit_floor' => $request->input('unit_floor'),
            'occupant_name' => $request->input('occupant_name'),
            'contact_number' => $request->input('contact_number')
        ]);
        return redirect('/units');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id)->first();

        return view('units.edit')->with('unit' , $unit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newUnitId = $request->input('unit_block') . '-' . $request->input('unit_floor') . '-' . $request->input('unit_number');
        Unit::where('id', $id)
            ->update([
                'occupant_name' => $request->input('occupant_name'),
                'contact_number' => $request->input('contact_number')
            ]);
        return redirect('/units');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $unit -> delete();

        return redirect('/units');
    }

    public function search(Request $request)
    {
        $results = Unit::where('occupant_name', 'like', '%' . $request->get('searchRequest') . '%')->get();

        return json_encode($results);
    }
}

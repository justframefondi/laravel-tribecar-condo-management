<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VisitorPostRequest;
use App\Models\Visitor;
use App\Models\Unit;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class VisitorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visitors = Visitor::all();
        return view('visitors.index', [
            'visitors' => $visitors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('visitors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitorPostRequest $request)
    {
        try {
            $visitorLimitCheck = $this->visitorLimit($request->input('unit_id'));
            if ($visitorLimitCheck > 8) {
                return view('visitors.warning',[
                    'name' => $request->input('name'),
                    'nric' => $request->input('nric'),
                    'phone' => $request->input('phone'),
                    'unit_id' => strtoupper($request->input('unit_id'))
                ]);
            } else {
                $visitor = Visitor::create([
                    'name' => $request->input('name'),
                    'nric' => $request->input('nric'),
                    'phone' => $request->input('phone'),
                    'unit_id' => strtoupper($request->input('unit_id'))
                ]);

                return redirect('/visitors');
            }
        } catch (\Exception $e) {
            print($e);
        }
    }

    public function forceStore(VisitorPostRequest $request)
    {
        try {
            $visitor = Visitor::create([
                'name' => $request->input('name'),
                'nric' => $request->input('nric'),
                'phone' => $request->input('phone'),
                'unit_id' => strtoupper($request->input('unit_id'))
            ]);
            return redirect('/visitors');
        } catch (\Exception $e) {
            print($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visitor = Visitor::find($id)->first();

        return view('visitors.edit')->with('visitor' , $visitor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $visitor = Visitor::where('id', $id)
        ->update([
            'name' => $request->input('name'),
            'nric' => $request->input('nric'),
            'phone' => $request->input('phone'),
            'unit_id' => $request->input('unit_id'),
            'entry_time' => $request->input('entry_time'),
            'exit_time' => $request->input('exit_time')
        ]);

        return redirect('/visitors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitor $visitor)
    {
        $visitor -> delete();

        return redirect('/visitors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exit($id) {
        $date = Carbon::now()->toDateTimeString();

        $visitor = Visitor::where('id', $id)
        ->update([
            'exit_time' => $date
        ]);

        return redirect('/visitors');
    }

    public function visitorLimit($unit_id) {
        $visitor = Visitor::where('unit_id', $unit_id);
        $visitor->whereNull('exit_time');
        $visitor->whereNull('deleted_at');
        $visitorCount = $visitor->count();
        return $visitorCount;
    }

    public function search(Request $request)
    {
        $results = Visitor::where('name', 'like', '%' . $request->get('searchVisitors') . '%')->get();
        return json_encode($results);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ExistingUnit;
class VisitorPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit_id' => [new ExistingUnit],
            'name' => ['required'],
            'phone' => ['required', 'numeric'],
            'nric' => ['required', 'min:2', 'max:3']
        ];
    }
}

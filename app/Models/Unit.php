<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'units';

    protected $primaryKey = 'id';

    protected $fillable = ['id','unit_block','unit_floor', 'unit_number', 'occupant_name', 'contact_number', 'deleted_at'];
}

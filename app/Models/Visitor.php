<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visitor extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'visitors';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'nric', 'phone', 'unit_id', 'entry_time', 'exit_time'];
}

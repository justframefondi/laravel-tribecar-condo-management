<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    //consists of block & unit number, occupant name, contact number
    {
        Schema::create('units', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->string('unit_block');
            $table->string('unit_number');
            $table->string('unit_floor');
            $table->string('occupant_name');
            $table->string('contact_number');
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}

<html>
    <body>
        <div class="container-footer">

            <p>
                Tribecar - management demo
            </p>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            $('body').on('keyup','#search-units', function() {
                let searchRequest = $(this).val()

                $.ajax({
                    method:"POST",
                    url: '/units/search',
                    dataType: 'json',
                    data: {
                        '_token': '{{ csrf_token() }}' ,
                        'searchRequest': searchRequest
                    },
                    success: function(res) {
                        let tableRow = '';
                        $('#dynamic-row').html('');
                        $.each(res, function (index, value) {
                            tableRow = `
                                        <div class="m-auto">
                                        <span class="uppercase text-blue-500 font-bold text-xs">
                                            <h2 class="uppercase text-blue-700 font-bold text-2xl italic" >Occupant Name:`+value.occupant_name+`</h2>
                                            <h2 class="text-gray-500 text-lg" >Block:`+ value.unit_block + `</h2>
                                            <h2 class="text-gray-500 text-lg" >Floor: `+ value.unit_floor +`</h2>
                                            <h2 class="text-gray-500 text-lg" >Unit: `+ value.unit_number +`</h2>
                                            <h2 class="text-gray-500 text-lg" >Contact Number:` + value.contact_number +`</h2>
                                            <h2 class="text-gray-500 text-lg" >Unit Created:`+ value.created_at +`</h2>
                                            <h2 class="text-gray-500 text-lg" >Unit Updated:`+ value.updated_at +`</h2>
                                        </span>
                                        </div>
                                        <div class="py-5">
                                        <div class="float-right text-gray-500"><a href="units/`+value.unit_block+`-`+value.unit_floor+`-`+value.unit_number+`}/edit">EDIT &rarr;</a></div>
                                        <br>
                                        <form class="float-right text-red-500" action="/units/`+value.unit_block+`-`+value.unit_floor+`-`+value.unit_number+`}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit">
                                            DELETE &rarr;
                                            </button>
                                        </form>
                                        </div>
                                        <hr class="mt-4 mb-8">`
                                $('#dynamic-row').append(tableRow)
                        });
                    }
                })
            })
        </script>

        <script type="text/javascript">
                    $('body').on('keyup','#search-visitors', function() {
                        let searchVisitors = $(this).val()
                        $.ajax({
                            method:"POST",
                            url: '/visitors/search',
                            dataType: 'json',
                            data: {
                                '_token': '{{ csrf_token() }}' ,
                                'searchVisitors': searchVisitors
                            },
                            success: function(res) {
                                console.log(res)
                                let tableRow = '';
                                let punchOutButton = ''
                                $('#dynamic-visitor-row').html('');
                                $.each(res, function (index, value) {
                                    if (value.exit_time ==null) {
                                        punchOutButton = `<form class="float-left" action="/visitors/`+value.id+`/exit" method="POST">
                                        @csrf
                                        <button type="submit" class="text-green-500">
                                            EXIT TIME &rarr;
                                        </button>
                                        </form>`
                                    }
                                    tableRow = `
                                    <div class="m-auto">
                                        <span class="uppercase text-blue-500 font-bold text-xs">
                                            <h2 class="uppercase text-blue-700 font-bold text-3xl italic" >Id: `+value.id+`</h2>
                                            <h2 class="uppercase text-blue-700 font-bold text-3xl italic" >Name: `+value.name+`</h2>
                                            <h2 class="text-gray-500 text-lg" >3D-NRIC: `+value.nric+`</h2>
                                            <h2 class="text-gray-500 text-lg" >Phone: `+value.phone+`</h2>
                                            <h2 class="text-gray-500 text-lg" >Unit: `+value.unit_id+`</h2>
                                            <h2 class="text-gray-500 text-lg" >Time Entered: `+value.entry_time+`</h2>
                                            <h2 class="text-gray-500 text-lg" >Time Exit: `+value.exit_time+`</h2>
                                        </span>
                                    </div>`+ punchOutButton +`
                                    <div class="py-5">
                                        <br>
                                        <div class="float-right text-gray-500"><a href="visitors/`+value.id+`/edit">EDIT &rarr;</a></div>
                                        <br>
                                        <form class="float-right text-red-500" action="/visitors/`+value.id+`" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit">
                                                DELETE &rarr;
                                            </button>
                                        </form>
                                    </div>
                                    <hr class="mt-4 mb-8">`

                                    $('#dynamic-visitor-row').append(tableRow)
                                });
                            }
                        })
                    })
        </script>
    </body>
</html>

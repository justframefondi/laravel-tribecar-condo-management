@extends('layouts.app')
@section('content')
<div class="m-auto w-4/5 py-24">
    <div class="text-center">
        <div class="text-5xl uppercase bold">
            <div class="pt-10">
                <a
                    href="units/create"
                    class="border-b-2 pb-2 border-dotted italic text-gray-500">
                    New unit Form &rarr;
                </a>
            </div>
        </div>
    </div>

     <!-- Search Widget -->
     <input type="text" id="search-units" class="form-control" placeholder="Start by searching...">


    <div class="w-5/6 py-10">
        <div id="dynamic-row">
            @foreach ($units as $unit)
            <div class="m-auto">
                <span class="uppercase text-blue-500 font-bold text-xs">
                    <h2 class="uppercase text-blue-700 font-bold text-2xl italic" >Occupant Name: {{$unit->occupant_name}}</h2>
                    <h2 class="text-gray-500 text-lg" >Block: {{$unit->unit_block}}</h2>
                    <h2 class="text-gray-500 text-lg" >Floor: {{$unit->unit_floor}}</h2>
                    <h2 class="text-gray-500 text-lg" >Unit: {{$unit->unit_number}}</h2>
                    <h2 class="text-gray-500 text-lg" >Contact Number: {{$unit->contact_number}}</h2>
                    <h2 class="text-gray-500 text-lg" >Unit Created: {{$unit->created_at}}</h2>
                    <h2 class="text-gray-500 text-lg" >Unit Updated: {{$unit->updated_at}}</h2>
                </span>
            </div>
            <div class="py-5">
                <div class="float-right text-gray-500"><a href="units/{{$unit->unit_block}}-{{$unit->unit_floor}}-{{$unit->unit_number}}/edit">EDIT &rarr;</a></div>
                <br>
                <form class="float-right text-red-500" action="/units/{{$unit->unit_block}}-{{$unit->unit_floor}}-{{$unit->unit_number}}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit">
                        DELETE &rarr;
                    </button>
                </form>
            </div>
                <hr class="mt-4 mb-8">
            @endforeach
        </div>
    </div>
</div>


@endsection

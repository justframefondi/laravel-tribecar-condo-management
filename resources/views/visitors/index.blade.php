@extends('layouts.app')
@section('content')
<div class="m-auto w-4/5 py-24">
    <div class="text-center">
        <div class="text-5xl uppercase bold">
            <div class="pt-10">
            <a
                href="visitors/create"
                class="border-b-2 pb-2 border-dotted italic text-gray-500">
                New Visitor Form &rarr;
            </a>
            </div>
        </div>
    </div>
     <!-- Search Widget -->
    <input type="text" id="search-visitors" class="form-control" placeholder="Start by searching...">


    <div class="w-5/6 py-10">
        <div id="dynamic-visitor-row">
            @foreach ($visitors as $visitor)
            <div class="m-auto">
                <span class="uppercase text-blue-500 font-bold text-xs">
                    <h2 class="uppercase text-blue-700 font-bold text-2xl italic" >Id: {{$visitor->id}}</h2>
                    <h2 class="uppercase text-blue-700 font-bold text-2xl italic" >Name: {{$visitor->name}}</h2>
                    <h2 class="text-gray-500 text-lg" >3D-NRIC: {{$visitor->nric}}</h2>
                    <h2 class="text-gray-500 text-lg" >Phone: {{$visitor->phone}}</h2>
                    <h2 class="text-gray-500 text-lg" >Unit: {{$visitor->unit_id}}</h2>
                    <h2 class="text-gray-500 text-lg" >Time Entered: {{$visitor->entry_time}}</h2>
                    <h2 class="text-gray-500 text-lg" >Time Exit: {{$visitor->exit_time}}</h2>
                </span>
            </div>

                <!-- if no exit time show exit button -->
            @if(!$visitor->exit_time == 'null')
            <form class="float-left" action="/visitors/{{ $visitor ->id }}/exit" method="POST">
                @csrf
                <button type="submit" class="text-green-500">
                    EXIT TIME &rarr;
                </button>
            </form>
            @endif
            <div class="py-5">
                <br>
                <div class="float-right text-gray-500"><a href="visitors/{{ $visitor->id }}/edit">EDIT &rarr;</a></div>
                <br>
                <form class="float-right text-red-500" action="/visitors/{{ $visitor ->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit">
                        DELETE &rarr;
                    </button>
                </form>
            </div>
            <hr class="mt-4 mb-8">
            @endforeach
        </div>
    </div>
</div>


@endsection

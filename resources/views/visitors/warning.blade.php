@extends('layouts.app')
@section('content')
<style>
      html, body {
      min-height: 100%;
      }
      body, div, form, input, select, p {
      padding: 0;
      margin: 0;
      outline: none;
      font-family: Roboto, Arial, sans-serif;
      font-size: 16px;
      color: #eee;
      }
      body {
      background-size: cover;
      }
      h1, h2 {
      text-transform: uppercase;
      font-weight: 400;
      }
      h2 {
      margin: 0 0 0 8px;
      }
      .main-block {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 100%;
      padding: 25px;
      background: rgba(0, 0, 0, 0.5);
      }
      .left-part, form {
      padding: 25px;
      }
      .left-part {
      text-align: center;
      }
      .fa-graduation-cap {
      font-size: 72px;
      }
      form {
      background: rgba(0, 0, 0, 0.7);
      }
      .title {
      display: flex;
      align-items: center;
      margin-bottom: 20px;
      }
      .info {
      display: flex;
      flex-direction: column;
      }
      input, select {
      padding: 5px;
      margin-bottom: 30px;
      background: transparent;
      border: none;
      border-bottom: 1px solid #eee;
      }
      input::placeholder {
      color: #eee;
      }
      option:focus {
      border: none;
      }
      option {
      background: black;
      border: none;
      }
      .checkbox input {
      margin: 0 10px 0 0;
      vertical-align: middle;
      }
      .checkbox a {
      color: #26a9e0;
      }
      .checkbox a:hover {
      color: #85d6de;
      }
      .btn-item, button {
      padding: 10px 5px;
      margin-top: 20px;
      border-radius: 5px;
      border: none;
      background: #26a9e0;
      text-decoration: none;
      font-size: 15px;
      font-weight: 400;
      color: #fff;
      }
      .btn-item {
      display: inline-block;
      margin: 20px 5px 0;
      }
      button {
      width: 100%;
      }
      button:hover, .btn-item:hover {
      background: #85d6de;
      }
      @media (min-width: 568px) {
      html, body {
      height: 100%;
      }
      .main-block {
      flex-direction: row;
      height: calc(100% - 50px);
      }
      .left-part, form {
      flex: 1;
      height: auto;
      }
      }

      .alert-danger {
          color: red;
      }
    </style>

<div class="main-block">
    <div class="left-part">
        <h1>Warning Visitor Limit Exceeded</h1>
        <a href='/visitors'><button>DENY</button></a>
    </div>
    <form action="/visitors/forceStore" method="POST">
    @csrf
        <div class="title">
            <i class="fas fa-pencil-alt"></i>
            <h2>Allow Visitor</h2>
        </div>
        <div class="info">
            <input type="text" type="text" name="name" value="{{$name}}">
                @error('name')
                    <div class="alert alert-danger"> {{ $message }}</div>
                @enderror

            <input type="text" name="nric" value="{{$nric}}">
                @error('nric')
                    <div class="alert alert-danger"> {{ $message }}</div>
                @enderror

            <input type="text" name="phone" value="{{$phone}}">
                @error('phone')
                    <div class="alert alert-danger"> {{ $message }}</div>
                @enderror

            <input type="text" name="unit_id" value="{{$unit_id}}">
                @error('unit_id')
                    <div class="alert alert-danger"> {{ $message }}</div>
                @enderror
        </div>
        <button type="submit" >Allow Visitor Entry</button>
    </form>
</div>

@endsection

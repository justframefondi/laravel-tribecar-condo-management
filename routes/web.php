<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VisitorsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UnitsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Homepage
Route::get('/',[PagesController::class,'index']);
//Visitor Route
Route::post('/visitors/{id}/exit', [VisitorsController::class, 'exit']);
Route::post('/visitors/forceStore', [VisitorsController::class, 'forceStore']);
Route::get('/visitors/visitorLimit/{unit_id}', [VisitorsController::class, 'visitorLimit']);
Route::post('/visitors/search', [VisitorsController::class, 'search']);
Route::resource('/visitors', VisitorsController::class);

//Units Route
Route::post('/units/search', [UnitsController::class, 'search']);
Route::resource('/units', UnitsController::class);
